from authenticator import authenticator
from queries.health_data import HealthDataRepository
from fastapi.testclient import TestClient
from main import app
from models.health_data_models import HealthDataOut

client = TestClient(app)


# Mock HealthDataRepository class
class MockHealthDataRepository:
    def get_all(self, user_id: int):
        # Mock implementation, return some dummy data
        return [
            HealthDataOut(
                id=1,
                user_id=user_id,
                height=170,
                weight=70,
                age=30,
                level="Advanced",
                goal="Lose weight",
                health_data_date="2024-02-07",
            )
        ]


# Override the HealthDataRepository dependency with the mock class


# Mock account data
def mock_get_current_account_data():
    return {
        "id": 1,
        "username": "test",
        "email": "test@example.com",
        "hashed_password":
        "$2b$12$Q8wagccMWsV0TkhcVTPt0u/Rq8oVT5H4HVycJRIruyrnMzwG4A7Um",
    }


def test_get_all_health_data():
    # Override the account dependency

    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_get_current_account_data
    app.dependency_overrides[HealthDataRepository] = MockHealthDataRepository
    # Make a request to the endpoint
    response = client.get("/api/health_data")

    client.dependency_overrides = {}

    # Check if the response data matches the expected format
    expected_data = [
        {
            "id": 1,
            "user_id": 1,
            "height": 170,
            "weight": 70,
            "age": 30,
            "level": "Advanced",
            "goal": "Lose weight",
            "health_data_date": "2024-02-07",
        }
    ]
    assert response.json() == expected_data
    assert response.status_code == 200
