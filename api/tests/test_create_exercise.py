from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.exercises import ExerciseRepository
from models.exercise_models import ExerciseOut

client = TestClient(app)


class MockExerciseData:
    def create(self, user_id, exercise):
        return ExerciseOut(
            id=1,
            user_id=user_id,
            name=exercise.name,
            type=exercise.type,
            muscle=exercise.muscle,
            equipment=exercise.equipment,
            difficulty=exercise.difficulty,
            instructions=exercise.instructions,
            sets=exercise.sets,
            reps=exercise.reps,
            time=exercise.time,
            exercise_date=exercise.exercise_date,
        )


def mock_get_account_data():
    return {"id": 1}


def test_create_exercise():
    app.dependency_overrides[ExerciseRepository] = MockExerciseData
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_get_account_data

    exercise_json = {
        "user_id": 1,
        "name": "string",
        "type": "string",
        "muscle": "string",
        "equipment": "string",
        "difficulty": "string",
        "instructions": "string",
        "sets": 1,
        "reps": 1,
        "time": 1,
        "exercise_date": "2024-01-30",
    }
    response = client.post("/api/exercises", json=exercise_json)

    app.dependency_overrides = {}

    expected_response = {
        "id": 1,
        "user_id": exercise_json["user_id"],
        "name": exercise_json["name"],
        "type": exercise_json["type"],
        "muscle": exercise_json["muscle"],
        "equipment": exercise_json["equipment"],
        "difficulty": exercise_json["difficulty"],
        "instructions": exercise_json["instructions"],
        "sets": exercise_json["sets"],
        "reps": exercise_json["reps"],
        "time": exercise_json["time"],
        "exercise_date": exercise_json["exercise_date"],
    }
    assert response.status_code == 200
    assert response.json() == expected_response
