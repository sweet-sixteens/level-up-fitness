from pydantic import BaseModel
from typing import Optional
from datetime import date


class HealthDataIn(BaseModel):
    height: Optional[int]
    weight: Optional[int]
    age: Optional[int]
    level: str
    goal: str
    health_data_date: date


class HealthDataOut(BaseModel):
    id: int
    user_id: int
    height: Optional[int]
    weight: Optional[int]
    age: Optional[int]
    level: str
    goal: str
    health_data_date: date
