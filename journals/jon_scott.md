## 16 Jan 2024

Coders log:

created journal and worked with sweet sixteen team to get posgresDB built.
Justin had his screen shared and he was writing the code while we helped check for any issues and did some trouble shooting to create tables and view tabels created in the database.

## 17 Jan 2024

Coders log:

Justin screen shared while we finished up building our DB had a couple issues we need to get a SEIR to help us trouble shoot it we had to delete our volume and images and container in docker then rebuild and everything was greenin docker.

Time to authenticate working in one breakout room Lucas screen shared and wrote the code we got a good way through the authentications but had to stop cause all the explorations and lecture used MongoDB and we are using postgres so we stopped at 1900 and will pick up tomorrow to get help and move forward with the authentication.

## 18 Jan 2024

Coders log:

Lucas screen shared while we figured out the authorization file to be able to log a user in with a token we had a SEIR Eli come in and help and we did some trouble shooting had to add an str to a param.

I screened shared and created the queries and routers exercise.py files and wrote the code to create an exercise, update migrations.py and main.py. we were able to see it in fastAPI but kept gettinga 400 error we pruned the dockeer files and created then that didnt were we refactored the code to try and fix it. the exercise was being created we could see it in the DB table be it would show in fasAPI so at 1400 I logged off and went to workout to clear my head.

## 19 Jan 2024

Coders log:

Screen shared to begin trouble shooting the validation error from yesterday we looked at more documentation and tried to refactor code again all of that didn't work. we enlisted some help from the SEIRS and we were not returning anything in our router.py.

coded put, delete, and get_all request had to trouble shoot a few 500 errors and 400 errors refactored code with SEIR Shashwath's help. handed the driving over to hecter so he could finish up the delete and code the get_one request today was a good day.

## 22 Jan 2024

Coders log:

Sreen shared to start coding health_data routers, queries, models, and main to create health data and get one instance of the health data, also updated migarations to create a health_data table. had to trouble shoot a couple minimal syntax errors.

Justin took over driving to finish up with the get all list of health data in queries and routers.

## 23 Jan 2024

Coders log:

Justin screen shared and we finished up the health_data get all function adding a date param in our database to auto generate a date and added it to our exercise.py.

Lucas took over to code the workout get list it worked and we are going to get Bart to check our error handling before we move on to the front end work. bart did come in and help us with our error handling which hector refactored our code we need to raise an exception that we were not doing and after adding the error handling we were able to get the fast api working correctly.

## 24 Jan 2024

Coders log:

we have made it to the front end hector is working with redux to build our front end login, logout, and signup page we are rewatching rileys video on redux having an issue figuring out jsx files versus js files and getting bootstrap to load finally got it to load after changing the bootstrap import statement, created slice

## 25 Jan 2024

Coders log:

justin worked on the unit test and we got the same error due to the test not finding the module main. we decided to take a break and catch up on some explorations.

## 26 Jan 2024

Coders log:

justin shared screen after refactoring the unit test code after our lecture on unit tests with delonte and we are running in to the same error where the test can't find the main module. had a SEIR and and instructor come in the room to help with the error we were unable to figure it out so we moved on to doing a different unit test.

Lucas Shared screen to code a unit test for getting health data while watching the unit test recording lecture was not able to get the test to work had SEIR help but we were still unbale to parse the code and refactor it.

## 29 Jan 2024

Coders log:

Took the drivers seat to share screen and build the home page for our web page it has a carousel with hard coded images for now just to see what it would look like we have card under the carousel on the side of our login form the has a slide feature for users to either login or signup. it was good to try things in css to move things around and add space and manipulate our cards and forms.

## 30 Jan 2024

Coders log:

Justin wrote the code to get our signup inputs working.
Bart came in to help us with signup form issues we need to do some error handling for duplicate accounts and we had a grammatical error that was found and fixed.

I have been working on the login inputs I think I have them implemented properly but they need to redirect to the user page.

## 31 Jan 2024

Coders log:

Justin commited his signup code and merged to main I commited my login code it appers to work but it still needs to redirect and if the password is wrong it needs an error message.

hector shared screen to approve the merge request and is now working on the error message for the confirmed password doesn't match the password and we decided to not use redux so we had to change the code to handle the login in the Homepage.jsx.

broke up some of the work and I created the workout page jsx and did some bootstrap styling to have an idea of what th page might look like.

## 01 Feb 2024

Coders log:

we all worked on different parts of the projet hector had the calender, justin the redirect for the login and error handling, lucas did the health data page so it would only show for logged in users, and I finished up the styling of the workout page I had to make more customized classes in .css files so that they would effect the styling globally.

## 05 Feb 2024

Coders log:

Started writing the code to get the drop downs to work so that it will send a request to the third party api to grb the information we need to get an exercise. had to create a new exercises_thirdp_request.py to put the code in to send the request to the right URL, updated the routers to send to the back un to make that request, and created new thirdp models to set the params.

## 06 Feb 2024

Coders log:

screen shared to refactor code to try and get workout-page to get exercises from the third party API, i did get that to work with Justin's help. worked with SEIRs to get workou-page to populate the data to send to the database to post an exercise but I am still getting errors we needed to get the user id from the backend and not the front end.

## 07 Feb 2024

Coders log:

coded the unit test for get_one exercise on ## Dev Summary

In this MR I'm adding the following functionality:

-   unit test
    -   this will test if our get_one exercise is working.

I made the following changes:

-   added the word try to get_current_account_data now read try_get_current_account_data
    -   so the test would work

## Test Plan

Screenshots or working page:

(put screenshot here)

For these backend changes, please follow the below steps to ensure my code works:

1. hit endpoint with postman, localhost:8000/docs page, or curl request like

```
in docker fastapi exec tab type python-m pytest to run test
```

2. expect successful response (without password) like below:

```
{ test passed }
```

## Resources

-   [add links to articles/youtubes/etc. you used to complete your task](stackoverflow.com)

## This MR is ready to submit because:

-   [x] It is up-to-date with main
-   [x] The "merge" option is selected
-   [x] I have removed any commented or unused code
-   [ ] The gitlab issue is linked to this MR

## 08 Feb 2024

Coders log:

this morning merged workout page with main had hector approve the merge to get all code updated to most current. now working on workoutPage to get it to post a workout to the database so we can get a list of exercises for a workout and have it connect with the calender the hector is working on.

In this MR I'm adding the following functionality:

backend endpoint router

added try except to get an error



I made the following changes:
try:
user_exercises = repo.get_all(user_id=account["id"])
print(user_exercises, "why isnt this working###########")
except Exception:
raise HTTPException(
status_code=status.HTTP_400_BAD_REQUEST,
detail="Cannot get exercise",
)
return user_exercises

Test Plan
For these frontend changes, please follow the below steps to ensure my code works:


http://localhost:5173/workout_page (or the initial page your test starts on)


click inputs and make a selection
before: didn't get list exercises
now: I get a list of exercise when I submit


Screenshots or working page:
(put screenshot here)


## Resources

- [add links to articles/youtubes/etc. you used to complete your task](stackoverflow.com)

## This MR is ready to submit because:

- [X] It is up-to-date with main
- [X] The "merge" option is selected
- [X] I have removed any commented or unused code


did some more styling for the pages added a logo in th enave bar

## 09 Feb 2024

Coders log:

Had to add logic to turn a str into an int the browser wants a string but the BD wants an int or null.

added this to make that work

const formDataForSubmit = () => {
        if (formData.sets === '') {
            formData.sets = null
        } else {
            parseInt(formData.sets)
        }
        if (formData.reps === '') {
            formData.reps = null
        } else {
            parseInt(formData.reps)
        }
        if (formData.time === '') {
            formData.time = null
        } else {
            parseInt(formData.time)
        }
        console.log(formData.sets, formData.reps, formData.time)
    }

and called it in the handle submit
