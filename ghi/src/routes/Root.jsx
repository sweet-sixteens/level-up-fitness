import { Outlet } from 'react-router-dom'
import { AuthProvider } from '@galvanize-inc/jwtdown-for-react'
import MyNavBar from '../components/Nav'

const API_HOST = import.meta.env.VITE_API_HOST_FROM_ENV

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

export default function Root() {
    return (
        <div>
            <AuthProvider baseUrl={API_HOST}>
                <MyNavBar />
                <Outlet />
            </AuthProvider>
        </div>
    )
}
