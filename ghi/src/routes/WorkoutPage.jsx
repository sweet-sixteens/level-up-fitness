import { useEffect, useState } from 'react'
import '../css/WorkoutPage.css'
import '../css/Logo.css'
import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import { useLocation } from 'react-router-dom'

const API_HOST = import.meta.env.VITE_API_HOST_FROM_ENV

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

function WorkoutPage() {
    const { token } = useAuthContext()
    const { state } = useLocation()
    const [editClicked, setEditClicked] = useState(false)
    const [editId, setEditId] = useState('')
    const [muscle, setMuscle] = useState('')
    const [userExercises, setUserExercises] = useState([])
    const [exercise, setExercise] = useState({})
    const [exercises, setExercises] = useState([])
    const [score, setScore] = useState('')
    const [formData, setFormData] = useState({
        name: '',
        type: '',
        muscle: '',
        equipment: '',
        difficulty: '',
        instructions: '',
        sets: '',
        reps: '',
        time: '',
        exercise_date: state.date,
    })

    const getData = async (muscle) => {
        const exerciseUrl = `${API_HOST}/api/thirdp_exercises/${muscle}`
        const response = await fetch(exerciseUrl, {
            headers: { Authorization: `Bearer ${token}` },
        })
        if (response.ok) {
            const apiExercises = await response.json()
            setExercises(apiExercises)
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name
        setFormData({
            ...formData,
            [inputName]: value,
        })
    }

    const formDataForSubmit = () => {
        if (formData.sets === '') {
            formData.sets = null
        } else {
            parseInt(formData.sets)
        }
        if (formData.reps === '') {
            formData.reps = null
        } else {
            parseInt(formData.reps)
        }
        if (formData.time === '') {
            formData.time = null
        } else {
            parseInt(formData.time)
        }
    }

    const handleMuscleSelect = (event) => {
        const value = event.target.value
        setMuscle(value)
        getData(value)
    }

    const handleExerciseSelect = (event) => {
        const value = JSON.parse(event.target.value)
        setExercise({
            name: value.name,
            type: value.type,
            muscle: value.muscle,
            equipment: value.equipment,
            difficulty: value.difficulty,
            instructions: value.instructions,
        })
        setFormData((previousFormData) => ({
            ...previousFormData,
            name: value.name,
            type: value.type,
            muscle: value.muscle,
            equipment: value.equipment,
            difficulty: value.difficulty,
            instructions: value.instructions,
        }))
    }

    const handleScoreSelect = (event) => {
        const value = event.target.value
        setScore(value)
    }

    const muscles = [
        'abdominals',
        'abductors',
        'adductors',
        'biceps',
        'calves',
        'chest',
        'forearms',
        'glutes',
        'hamstrings',
        'lats',
        'lower_back',
        'middle_back',
        'neck',
        'quadriceps',
        'traps',
        'triceps',
    ]

    const scores = ['sets', 'reps', 'time']

    useEffect(() => {
        if (token) {
            fetchData()
        }
    }, [token])

    const fetchData = async () => {
        const fetchConfig = {
            method: 'get',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        }
        const request = await fetch(
            `${API_HOST}/api/workouts/${state.date}`,
            fetchConfig
        )
        if (request.ok) {
            const data = await request.json()
            setUserExercises(data)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        formDataForSubmit()

        const exerciseUrl = `${API_HOST}/api/exercises`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(exerciseUrl, fetchConfig)
        if (response.ok) {
            setFormData({
                name: '',
                type: '',
                muscle: '',
                equipment: '',
                difficulty: '',
                instructions: '',
                sets: '',
                reps: '',
                time: '',
                exercise_date: state.date,
            })
            setMuscle('')
            setExercises([])
            setScore('')
        }
        fetchData()
    }

    const handleEdit = async (id) => {
        const exerciseToEdit = userExercises.find(
            (exercise) => exercise.id === id
        )

        if (exerciseToEdit.sets === null) {
            exerciseToEdit.sets = ''
        } else {
            parseInt(exerciseToEdit.sets)
        }
        if (exerciseToEdit.reps === null) {
            exerciseToEdit.reps = ''
        } else {
            parseInt(exerciseToEdit.reps)
        }
        if (exerciseToEdit.time === null) {
            exerciseToEdit.time = ''
        } else {
            parseInt(exerciseToEdit.time)
        }

        ;(formData.name = exerciseToEdit.name),
            (formData.type = exerciseToEdit.type),
            (formData.muscle = exerciseToEdit.muscle),
            (formData.equipment = exerciseToEdit.equipment),
            (formData.difficulty = exerciseToEdit.difficulty),
            (formData.instructions = exerciseToEdit.instructions),
            (formData.sets = exerciseToEdit.sets),
            (formData.reps = exerciseToEdit.reps),
            (formData.time = exerciseToEdit.time),
            (formData.exercise_date = exerciseToEdit.exercise_date),
            setMuscle(exerciseToEdit.muscle)
    }

    useEffect(() => {
        if (muscle) {
            getData(muscle)
        }
    }, [muscle])

    const handleEditClick = async (event) => {
        event.preventDefault()
        formDataForSubmit()
        let id = editId
        const exerciseUrl = `http://localhost:8000/api/exercises/${id}`
        const fetchConfig = {
            method: 'put',
            body: JSON.stringify(formData),
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(exerciseUrl, fetchConfig)
        if (response.ok) {
            setFormData({
                name: '',
                type: '',
                muscle: '',
                equipment: '',
                difficulty: '',
                instructions: '',
                sets: '',
                reps: '',
                time: '',
                exercise_date: state.date,
            })
            setMuscle('')
            setExercises([])
            setScore('')
            setEditClicked(false)
        }
        fetchData()
    }

    const handleDelete = async (id) => {
        const request = await fetch(`${API_HOST}/api/exercises/${id}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
        fetchData()
    }

    return (
        <>
            <label className="fs-1 fw-bold ml">
                YOUR WORKOUT FOR {state.date}
            </label>
            <div className="card mx-auto" style={{ width: '60rem' }}>
                <table className="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Exercise</th>
                            <th scope="col">Instructions</th>
                            <th scope="col">Reps</th>
                            <th scope="col">Sets</th>
                            <th scope="col">Time</th>
                            <th className="text-center" colSpan="2">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {userExercises.map((userExercise) => {
                            return (
                                <tr key={userExercise.id}>
                                    <td>{userExercise.name}</td>
                                    <td>{userExercise.instructions}</td>
                                    <td>{userExercise.reps}</td>
                                    <td>{userExercise.sets}</td>
                                    <td>{userExercise.time}</td>
                                    <td>
                                        <button
                                            onClick={() => {
                                                setEditClicked(true)
                                                handleEdit(userExercise.id)
                                                setEditId(userExercise.id)
                                            }}
                                            className="btn btn-danger workout-page-btn"
                                        >
                                            Edit
                                        </button>
                                    </td>
                                    <td>
                                        <button
                                            onClick={() => {
                                                handleDelete(userExercise.id)
                                            }}
                                            className="btn btn-danger workout-page-btn"
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
            <div>
                <form
                    id="create-exercise-form"
                    className="mx-auto margin"
                    style={{ width: '60rem' }}
                >
                    <div className="input-group mb-3">
                        <select
                            onChange={handleMuscleSelect}
                            required
                            value={muscle}
                            id="muscle"
                            name="muscle"
                            className="form-select"
                        >
                            <option
                                className="workout-page-btn span-btn"
                                value=""
                            >
                                Choose a muscle
                            </option>
                            {muscles.map((muscle, index) => {
                                return (
                                    <option
                                        onClick={(muscle) => setMuscle(muscle)}
                                        key={index}
                                        value={muscle}
                                    >
                                        {muscle}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="input-group mb-3">
                        <select
                            onChange={handleExerciseSelect}
                            required
                            value={JSON.stringify(exercise)}
                            id="exercise"
                            name="exercise"
                            className="form-select"
                        >
                            <option
                                className="workout-page-btn span-btn"
                                value=""
                            >
                                Choose a exercise
                            </option>
                            {exercises.map((exercise, index) => {
                                return (
                                    <option
                                        onClick={(exercise) =>
                                            setExercise(exercise)
                                        }
                                        key={index}
                                        value={JSON.stringify(exercise)}
                                    >
                                        {exercise.name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="input-group mb-3">
                        <select
                            onChange={handleScoreSelect}
                            required
                            value={score}
                            id="score"
                            name="score"
                            className="form-select"
                        >
                            <option
                                className="workout-page-btn span-btn"
                                value=""
                            >
                                Choose a score type
                            </option>
                            {scores.map((score, index) => {
                                return (
                                    <option
                                        onClick={(score) => setScore(score)}
                                        key={index}
                                        value={score}
                                    >
                                        {score}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    {score && score === 'sets' ? (
                        <>
                            <div className="input-group mb-3">
                                <span
                                    className="input-group-text workout-page-btn span-btn"
                                    id="inputGroup-sizing-lg"
                                >
                                    Sets
                                </span>
                                <input
                                    onChange={handleFormChange}
                                    type="number"
                                    name="sets"
                                    value={formData.sets}
                                    id="sets"
                                    className="form-control"
                                    aria-label="Sizing example input"
                                    aria-describedby="inputGroup-sizing-lg"
                                />
                            </div>
                            <div className="input-group mb-3">
                                <span
                                    className="input-group-text workout-page-btn span-btn"
                                    id="inputGroup-sizing-lg"
                                >
                                    Reps
                                </span>
                                <input
                                    onChange={handleFormChange}
                                    type="number"
                                    name="reps"
                                    value={formData.reps}
                                    id="reps"
                                    className="form-control"
                                    aria-label="Sizing example input"
                                    aria-describedby="inputGroup-sizing-lg"
                                />
                            </div>
                        </>
                    ) : null}
                    {score && score === 'reps' ? (
                        <div className="input-group mb-3">
                            <span
                                className="input-group-text workout-page-btn span-btn"
                                id="inputGroup-sizing-lg"
                            >
                                Reps
                            </span>
                            <input
                                onChange={handleFormChange}
                                type="number"
                                name="reps"
                                value={formData.reps}
                                className="form-control"
                                aria-label="Sizing example input"
                                aria-describedby="inputGroup-sizing-lg"
                            />
                        </div>
                    ) : null}
                    {score && score === 'time' ? (
                        <>
                            <div className="input-group mb-3">
                                <span
                                    className="input-group-text workout-page-btn span-btn"
                                    id="inputGroup-sizing-lg"
                                >
                                    Reps
                                </span>
                                <input
                                    onChange={handleFormChange}
                                    type="number"
                                    name="reps"
                                    value={formData.reps}
                                    className="form-control"
                                    aria-label="Sizing example input"
                                    aria-describedby="inputGroup-sizing-lg"
                                />
                            </div>
                            <div className="input-group mb-3">
                                <span
                                    className="input-group-text workout-page-btn span-btn"
                                    id="inputGroup-sizing-lg"
                                >
                                    Time
                                </span>
                                <input
                                    onChange={handleFormChange}
                                    type="number"
                                    name="time"
                                    placeholder="time in seconds"
                                    value={formData.time}
                                    className="form-control"
                                    aria-label="Sizing example input"
                                    aria-describedby="inputGroup-sizing-lg"
                                />
                            </div>
                        </>
                    ) : null}
                    {editClicked ? (
                        <button
                            onClick={handleEditClick}
                            type="submit"
                            className="btn workout-page-btn"
                        >
                            Update workout
                        </button>
                    ) : (
                        <button
                            onClick={handleSubmit}
                            type="submit"
                            className="btn workout-page-btn"
                        >
                            Add to workout
                        </button>
                    )}
                </form>
            </div>
        </>
    )
}

export default WorkoutPage
